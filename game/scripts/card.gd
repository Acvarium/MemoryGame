extends Sprite
export var ID = 3
const maxID = 4
var state = true
var main_node

func _ready():
	main_node = get_node("/root/main")
	randomize()
	updateImage()

func _input(event):
	if Input.is_action_just_pressed("cheee"):
		$cover.self_modulate.a = 0.8
		
	if Input.is_action_just_released("cheee"):
		$cover.self_modulate.a = 1
		

func updateImage():
	var imageName = "res://textures/" + str("%03d" % ID) + ".png"
	$image.texture = load(imageName)

func close():
	if !state:
		$a.play_backwards("open");
		state = true
	
func _on_Button_pressed():
	if main_node.open(self):
		if state:
			$a.play("open");
			$chpok.play()
#			ID = randi()%maxID+1
#			updateImage()
		state = false
	else:
		$no.play()
