extends Control
export var opened = [null,null]
export var playes = ["Ira", "Vit"]
var pScores = [0,0]
var pInGame = 2
var pNum = 0

var cardObj = load("res://objects/card.tscn")
var fadeObj = load("res://objects/fadeOut.tscn")

var hSize = 10
var pairs = 25
var gridStep = 120
var screenSize = Vector2(1280,728)
var timeStart = false

var removePair = false
var score = 0
var step = 0
var time = 0.0
var EOG = false

func updatePScore():
	$players/p1Score.text = str(pScores[0])
	$players/p2Score.text = str(pScores[1])
	
	
func _ready():
	$topElements/score.text = "Pairs: 0/" + str(pairs)
	randomize()
	generate()
	updatePScore()
	
func _input(event):
	if Input.is_action_just_pressed("restart"):
		get_tree().reload_current_scene()

func _process(delta):
	if !EOG and timeStart:
		time += delta
		var seconds = int(time) % 60
		var minutes = int(time) % 3600 / 60
		var str_elapsed = "%02d:%02d" % [minutes, seconds]
		$topElements/time.text = str_elapsed

func generate():
	var pack = []
	for i in range(pairs):
		for j in range(2):
			pack.append(i)
	var col = 0
	var row = 0
	for i in range(pairs * 2):
		var card = cardObj.instance()
		$table.add_child(card)
		card.position = Vector2(col * gridStep, row * gridStep)
		var fromPack = randi()%pack.size()
		card.ID = pack[fromPack]
		card.updateImage()
		pack.remove(fromPack)
		col += 1
		if col >= hSize:
			col = 0
			row += 1
	
func open(card):
	timeStart = true
	for i in range(opened.size()):
		if opened[i] == null:
			opened[i] = card
			if i == opened.size() - 1:
				step += 1
				$topElements/step.text = "Step " + str(step)
				if (opened[0].ID == opened[1].ID):
					$sounds/futj.play()
					removePair = true
					score += 1
					pScores[pNum] += 1
					updatePScore()
					$topElements/score.text = "Pairs: " + str(score) + "/" + str(pairs)
					if $table.get_child_count() <= 2:
						$sounds/win.play()
						EOG = true
				pNum += 1
				if pNum >= pInGame:
					pNum = 0
				$closeCards.start()
			return true
		elif opened[i] == card:
			return false
	return false
	
func _on_closeCards_timeout():
	$players/currentP.text = playes[pNum]
	for i in range(opened.size()):
		if opened[i] != null:
			if removePair:
				$sounds/pair.play()
				var fade = fadeObj.instance()
				$effects.add_child(fade)
				fade.global_position = opened[i].global_position
				opened[i].queue_free()
			else:
				opened[i].close()
			opened[i] = null
	removePair = false
	if EOG:
		$newGame.show()

func _on_newGame_pressed():
	get_tree().reload_current_scene()


func _on_restartButton_pressed():
	get_tree().reload_current_scene()

func _on_menu_pressed():
	$menuPanel.visible = true

func _on_canc_pressed():
	$menuPanel.visible = false

func _on_switchPlayer_pressed():
	if timeStart:
		return
	pNum += 1
	if pNum >= pInGame:
		pNum = 0
	$players/currentP.text = playes[pNum]

